package ru.t1.yakovlev.tm;

import static ru.t1.yakovlev.tm.constant.TerminalConstant.*;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) return;

        processArgument(args[0]);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;

        switch (argument) {
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("email: aaykovlev@t1-consulting.ru.");
        System.out.println("name: Yakovlev Anton.");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show developer info.\n", ABOUT);
        System.out.printf("%s - show application version.\n", VERSION);
        System.out.printf("%s - show arguments description.\n", HELP);
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.println("Passed argument not recognized..");
        System.err.println("Try 'java -jar task-manager.jar help' for more information.");
    }

}
